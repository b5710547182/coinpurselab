package coinpurse;

import java.util.ResourceBundle;

public abstract class MoneyFactory {

	private static MoneyFactory instance;

	protected MoneyFactory()
	{
		
	}
	

	public Valuable createMoney(double value){return null;}
	
	
	public static MoneyFactory getInstance()
	{
		ResourceBundle bundle = ResourceBundle.getBundle( "data" );
		String factoryclass = bundle.getString( "moneyfactory" );  
		if(factoryclass==null)
		{
			instance = new ThaiMoneyFactory();
		}
		System.out.println("Factory class is " + factoryclass);
		try {
		    instance = (MoneyFactory)Class.forName(factoryclass).newInstance();
		} catch (Exception ex) {
		    System.out.println("Error creating MoneyFactory "+ex.getMessage() );
		    return new ThaiMoneyFactory();
		}

		if(instance==null)
			instance = new ThaiMoneyFactory();
		return instance;
	}
	
	
	public Valuable createMoney(String value)
	{
		double money = Double.parseDouble(value);
		return createMoney(money);
	}
	public static void setFactory(MoneyFactory factory)
	{
		instance = factory;
				
	}
	
	
	

}
