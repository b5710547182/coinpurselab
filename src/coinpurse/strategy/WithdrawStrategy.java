package coinpurse.strategy;

import java.util.List;
import coinpurse.Valuable;

/**
 * This interface will be the deciding on which withdrawn type will be used.
 * @author Thanawit Gerdprasert
 *
 */
public interface WithdrawStrategy {
	/**
	 * Withdrawn money from the List of Valuable Object.
	 * @param amoun� of the money that will be withdrawn.
	 * @param items from the list that will be withdrawn.
	 */
	public List<Valuable> withdraw(double amoun� , List<Valuable> items );
}
