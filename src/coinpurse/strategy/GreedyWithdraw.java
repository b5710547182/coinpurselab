package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;
/**
 * This class will withdraw the item as huge as possible for the amount.
 * @author Thanawit Gerdprasert
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {

	@Override
	public List<Valuable> withdraw(double amoun�, List<Valuable> coinPurse) {
		MyComparator test = new MyComparator();
		Collections.sort(coinPurse, test);
		int counter =0;
		ArrayList<Valuable> templist = new ArrayList<Valuable>();
		ArrayList<Valuable> index = new ArrayList<Valuable>();
		for(int i =0 ; i<coinPurse.size();i++)
		{
			if(amoun�>=coinPurse.get(i).getValue())
			{
				amoun�-=coinPurse.get(i).getValue();
				templist.add(coinPurse.get(i));
				counter++;
				index.add(coinPurse.get(i));
			}
		}
		if(amoun�!=0)
		{
			return null;
		}
		for (int i =0 ; i<counter;i++)
		{
			coinPurse.remove(templist.get(i));
		}

		return templist;
		
	}

	
}
