package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;

public class RecursiveWithdraw implements WithdrawStrategy {

	private int loopCounter = 0;
	private double leftover =0 ;
	private ArrayList<Valuable> imaginaryList = new ArrayList<Valuable>();
	
	@Override
	public List<Valuable> withdraw(double amount , List<Valuable> items)
	{
		MyComparator test = new MyComparator();
		Collections.sort(items, test);
		loopCounter =0;
		leftover = amount;
		List<Valuable> temp = withdrawFrom(amount,items,0);
		
		return temp;
	}
	/**
	 * Use to compute the List of Valuable and return to be withdrawn.
	 * @param amount of money that will be withdrawn.
	 * @param items of the Valuable Object.
	 * @param runner
	 * @return
	 */
	private List<Valuable> withdrawFrom(double amount,List<Valuable> items,int runner)
	{
		for( ; loopCounter<items.size(); )
		{
			if(amount-items.get(runner).getValue()==0)
			{
				
				amount-=items.get(runner).getValue();
				imaginaryList.add(items.get(runner));
				return imaginaryList;
			}
			else if(runner==items.size()-1&&amount!=0)
			{
				amount = leftover;
				loopCounter++;
				imaginaryList = new ArrayList<Valuable>();
				runner =loopCounter;
				return withdrawFrom(amount, items, runner);
			}
			else if(amount-items.get(runner).getValue()>0)
			{
				amount-=items.get(runner).getValue();
				imaginaryList.add(items.get(runner));
				runner++;
				return withdrawFrom(amount, items, runner);
			}
			else if(amount-items.get(runner).getValue()<0)
			{
				runner++;
				return withdrawFrom(amount, items, runner);
			}
		}
		return null;
		
	}
	
	
	
	
}
