package coinpurse;

import java.util.HashMap;
import java.util.Map;

/**
 * Coupon class which value is depends on its color.
 * @author Thanawit Gerdprasert
 *
 */
public class Coupon extends AbstractValuable{
	static Map<String,Double> numbers;
	private String color;
	static{
		numbers = new HashMap<String,Double>( );
		numbers.put("red", 100.00);
		numbers.put("green", 20.00);
		numbers.put("blue", 50.00);
	}
	/**
	 * Create the coupon object.
	 * @param color of the Coupon 
	 */
	public Coupon(String color,String currency)
	{	
		super(numbers.get(color), currency);
		this.color = color.toLowerCase();
	}
	
	/**
	 * Show the value of this coin in the String data type.
	 * @return String of value of this Coin
	 * 
	 */
	public String toString()
	{
		return this.color + " coupon";
	}
}
