package coinpurse;


/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Thanawit Gerdprasert
 */

public class Coin extends AbstractValuable {
	private String currency;
	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 */
	public Coin( double value,String currency ) {
		super(value,currency);
	}

	/**
	 * Show the value of this coin in the String data type.
	 * @return String of value of this Coin
	 * 
	 */
	public String toString ()
	{
		return String.format("%.0f %s Coin", this.getValue(), super.getValue());
	}


	



}