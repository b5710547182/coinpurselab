package coinpurse; 

import javax.swing.JFrame;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Thanawit Gerdprasert
 */
// test case 9 5 4 3 2 withdraw 15
public class Main extends JFrame {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
  
        // 1. create a Purse
    	Purse purse = new Purse(10);
        // 2. create a ConsoleDialog with a reference to the Purse object
    	purse.addObserver(new BalanceObserver());
    	purse.addObserver(new ProgressObserver(10));
    	ConsoleDialog test = new ConsoleDialog(purse);
    	//2.5 set the observer for Main
        // 3. run() the ConsoleDialog
    	test.run();
    }
    
    public void run()
    {
    	
    	
    	
    	
    	
    	
    }
}
