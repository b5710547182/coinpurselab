package coinpurse;

/**
 * BankNote class for the BankNote that has its own serial number and the value.
 * @author Thanawit Gerdprasert
 *
 */
public class BankNote extends AbstractValuable{

	
	private static int nextSerialNumber = 1000000;
	private int originalSerial;
	
	/**
	 * Create the BankNote with specific serial and value.
	 * @param value of the BankNote
	 */
	public BankNote(double value, String currency)
	{
		super(value,currency);
		this.originalSerial = nextSerialNumber++;
	}
	
	/**
	 * Return the serial number of the data.
	 * @return the serial number of this BankNote
	 */
	public int getNextSerialNumber()
	{
		return this.originalSerial;
	}
	
	/**
	 * Show the value of this coin in the String data type.
	 * @return String of value of this Coin
	 * 
	 */
	public String toString()
	{
		return this.getValue()+" "+super.getCurrency()+" Banknote";
	}

	

	
}
