package coinpurse;
	/**
	 *Use this interface to check the value of the Object that implements it.
	 * @author Thanwit Gerdprasert
	 *
	 */
public interface Valuable extends Comparable {
	/**
	 * Called by the object and return the value of its own.
	 * @return return the value of that object.
	 */
	public double getValue();
	
	/**
	 *give the information about this Object in String data type.  
	 * @return String that can give the information about itself.
	 */
	public String toString();
	
	/**
	 * check whether or not these two object are the same or not.
	 * @param  obj that will be compared with
	 * @return true if they have the same value else return false.
	 */
	public boolean equals(Object obj);
	
	/**
	 * Use this method to sort the data between two Valuable.
	 * @param obj is the object that will be compare.
	 * @return return 1 if a is > b return 0 if it's the same else return -1.
	 */
	public int compareTo (Object obj);
	
}
