package coinpurse;

public class ThaiMoneyFactory extends MoneyFactory{

	protected ThaiMoneyFactory() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public Valuable createMoney(double value) {
		if(value==1)
			return new Coin(1,"Baht");
		else if(value==5)
			return new Coin(5,"Baht");
		else if(value==10)
			return new Coin(10,"Baht");
		else if(value==20)
			return new BankNote(20,"Baht");
		else if(value==50)
			return new BankNote(50,"Baht");
		else if(value==100)
			return new BankNote(100,"Baht");
		else if(value==500)
			return new BankNote(500,"Baht");
		else if(value ==1000)
			return new BankNote(1000,"Baht");
		else 
			throw new IllegalArgumentException();
	}
	

}
