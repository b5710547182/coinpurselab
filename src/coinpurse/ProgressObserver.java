package coinpurse;

import java.awt.Container;
import java.awt.FlowLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
/**
 * This class get notified when it initialize deposit and withdraw and show Progress bar.
 * @author Thanawit Gerdprasert.
 */
public class ProgressObserver extends JFrame implements Observer {
	JProgressBar temp;
	JLabel data ;
	/**
	 * Update the progress bar and check the purse capacity whether it full or not.
	 * @param o the object that got recieved from the Object that caleld it.
	 * @param arg is used to check the condition of special event.
	 */
	public void update(Observable o, Object arg) 
	{
		Purse purse = (Purse) o ; 
		if(purse.isFull())
		{
			data.setText("FULL");
			temp.setVisible(false);
		}
		else if(purse.count()==0)
		{
			data.setText("EMPTY");
			temp.setVisible(false);
		}
		else
		{
			temp.setValue(purse.count());
		}
	}
	/**
	 * Initialize the component and run the data for the first time.
	 * @param capacity is the size that purse can get.
	 */
	public ProgressObserver(int capacity)
	{
		initComponent(capacity);
		this.run();
	}
	/**
	 * Run the class and set visibility to be true.
	 */
	public void run()
	{
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 * initialize the component of the GUI.
	 * @param capacity of the wallet.
	 */
	public void initComponent(int capacity)
	{
		this.setLayout(new FlowLayout());
		Container box1 = new Container();
		box1.setLayout(new FlowLayout());
		temp = new JProgressBar(0 , capacity);
		data = new JLabel();
		box1.add(temp);
		box1.add(data);
		this.add(box1);
		this.pack();
	}

	
}
