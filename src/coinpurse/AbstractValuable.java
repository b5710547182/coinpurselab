package coinpurse;


/**
 * Use this class as the main class for the other Valuable Object.
 * @author Thanawit Gerdprasert
 *
 */
public abstract class AbstractValuable implements Valuable {

	private double value;
	private String currency;
	
	/**
	 * The main Contructor for the different class that inherit from this class.
	 * @param value of the recieved item
	 */
	public AbstractValuable(double value,String currency)
	{
		this.value = value;
		this.currency = currency;
	}

	/**
	 * Call this method to compare two Coin Object and return whether it true or not.
	 * @param obj that will be test if equals or not.
	 * @return true if the coin has the same value, else return false
	 * @throws the Coin that will be compare is same class and not null
	 */
	public boolean equals(Object obj) {
		if(obj==null)
			return false;
		if ( obj.getClass() != this.getClass() )
			return false;
		Valuable other = (Valuable) obj;
		if ( this.getValue()==(other.getValue()  ) )
			return true;
		return false; 
	}

	/**
	 * Compare two coin to know which one is greater or less.
	 * @param   newCoin that will be compared
	 * @return 	return -1 if the parameter value has higher value, return 1 if parameter value has less value
	 * 			and return 0 if the parameter has the same value
	 */
	public int compareTo (Object newCoin) {
		Valuable other = (Valuable) newCoin;
		if(other==null)
		{
			throw new NullPointerException("the coin value is null");
		}
		else if(this.getValue()>other.getValue())
			return 1;
		else if (this.getValue()<other.getValue())
			return -1;
		else
			return 0;
	}
	@Override
	public double getValue() {
		return  this.value;
	}
	public String getCurrency(){
		return this.currency;
	}


}