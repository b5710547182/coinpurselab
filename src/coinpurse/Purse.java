package coinpurse; 
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Thanawit Gerdprasert
 */
public class Purse extends Observable {
	/** Collection of coins in the purse. */
    List<Valuable> coinPurse ;
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	private WithdrawStrategy strategy;
	
	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		coinPurse = new ArrayList<Valuable> (capacity);
	}

	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() 
	{ 
		return coinPurse.size();
	}

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() 
	{
		double balance =0;
		for(int i =0 ; i<coinPurse.size(); i++)
		{
			balance +=coinPurse.get(i).getValue();
		}
		return balance;
	}

	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	public int getCapacity() 
	{
		return this.capacity;
	}
	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full.
	 */
	public boolean isFull() {
		return (this.count()==this.getCapacity());
			
	}

	/** 
	 * Insert a coin into the purse.
	 * The coin is only inserted if the purse has space for it
	 * and the coin has positive value.  No worthless coins!
	 * @param value is a Coin object to insert into purse
	 * @return true if coin inserted, false if can't insert
	 */
	public boolean insert( Valuable value ) {
		if(isFull())
		{
			return false;
		}
		else if(value.getValue()<=0)
		{
			return false;
		}
		else
		{
			coinPurse.add(value);
			super.setChanged();
			super.notifyObservers();
			return true;
		}
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Coins withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		List<Valuable> tempNoConvert= strategy.withdraw(amount, coinPurse);
		if(tempNoConvert==null)
		{
			return null;
		}
		
		for(int i =0 ; i < tempNoConvert.size();i++)
		{
			coinPurse.remove(tempNoConvert.get(i));
		}
		Valuable[] returnValue = new Valuable[tempNoConvert.size()];
		
		
		for(int i =0 ; i<returnValue.length;i++)
		{
			returnValue[i] = tempNoConvert.get(i);
		}
		super.setChanged();
		super.notifyObservers();
		return returnValue;
		
	}
	/**
	 * set withdraw strategy for purse.
	 * @param temp the name of strategy.
	 */
	public void setWithdrawStrategy(WithdrawStrategy temp)
	{
		this.strategy = temp;
	}
	
	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return the String that can describe this object
	 */
	public String toString() {
		return this.count() + " Valuable with value "+this.getBalance();
	}

	
}