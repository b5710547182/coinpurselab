package coinpurse;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
/**
 * This class get notified when it initialize deposit and withdraw to show the Balance.
 * @author Thanawit Gerdprasert.
 */
public class BalanceObserver extends JFrame implements Observer {
	private JLabel amountOfMoney;
	
	/**
	 * Update the balance of the purse.
	 */
	public void update(Observable o, Object arg) {
		
		Purse purse = (Purse) o;
		amountOfMoney.setText(purse.getBalance()+"");
		
	}
	/**
	 * initialize the component of the GUI.
	 */
	private void initComponent()
	{	
		this.setLayout(new FlowLayout());
		Container box1 = new Container();
		box1.setLayout(new FlowLayout());
		box1.add(new JLabel("Welcome to Banking Service"));
		amountOfMoney = new JLabel();
		box1.add(amountOfMoney);
		this.add(box1);
		this.pack();

	}
	/**
	 * Run the class and set visibility to be true.
	 */
	public void run ()
	{
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	/**
	 *  Initialize the component and run the data for the first time.
	 */
	public BalanceObserver()
	{
		initComponent();
		this.run();
	}
	
	
}
