package coinpurse;
import java.util.Comparator;

/**
 * Call this interface to compare Valuable.
 * @author Thanawit Gerdprasert
 *
 */

public class MyComparator implements Comparator<Valuable>{

	/**
	 * Use this method to sort the data between two Valuable.
	 * @param a is the first unit to compare.
	 * @param b is the unit that will be compared.
	 * @return return 1 if a is > b return 0 if it's the same else return -1.
	 */
	public int compare(Valuable a, Valuable b) {
		if(a.getValue()>b.getValue())
			return -1;
		else if (a.getValue()<b.getValue())
			return 1;
		else
			return 0;
	}
}


