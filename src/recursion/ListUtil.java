package recursion;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ListUtil {
	public static void main (String [] args)
	{
		List<String> list;
		if(args.length>0)
			list = Arrays.asList(args);
		else
			list = Arrays.asList("bird","zebra","cat","pig");
		System.out.print("List contain ");
		
		printList(list); 
	}
	private static void printList(List<?> test)
	{	
		if(test.size()==1)
		{
			System.out.println(test.get(0));
		}
		else
		{
			System.out.print(test.get(0)+" , ");
			test.remove(0);
			printList(test);
		}	
			
	}

}
